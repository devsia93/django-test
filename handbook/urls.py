from rest_framework.routers import DefaultRouter

from handbook.views import HandbookViewSet
from handbook.views.handbook_item import HandbookItemViewSet

router = DefaultRouter()

router.register(r'handbooks', HandbookViewSet)
router.register(r'handbook_items', HandbookItemViewSet)

urlpatterns = [
    *router.urls,
]
