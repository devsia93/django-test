from django_filters import rest_framework

from rest_framework.viewsets import ReadOnlyModelViewSet

from handbook.models import Handbook
from handbook.serializers import BaseHandbookSerializer


class HandbookFilter(rest_framework.FilterSet):
    start_date__gte = rest_framework.DateFilter(field_name='start_date', lookup_expr='gte')


class HandbookViewSet(ReadOnlyModelViewSet):
    queryset = Handbook.objects.all()
    serializer_class = BaseHandbookSerializer
    filter_class = HandbookFilter
