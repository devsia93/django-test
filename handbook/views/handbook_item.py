from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from django.db.models import Q
from django_filters import rest_framework
from django.core.exceptions import ObjectDoesNotExist

from app.filter import CharInFilter, NumberInFilter
from handbook.models import HandbookItem, Handbook
from handbook.serializers import BaseHandbookItemSerializer, ValidateHandbookItemByTitleSerializer, \
    ValidateHandbookItemByVersionSerializer


class HandbookItemFilter(rest_framework.FilterSet):
    handbook = NumberInFilter(field_name='handbook', lookup_expr='in')
    handbook__version = CharInFilter(field_name='handbook__version', lookup_expr='in')
    handbook__title = rest_framework.CharFilter(field_name='handbook__title', lookup_expr='exact')


class HandbookItemViewSet(ReadOnlyModelViewSet):
    queryset = HandbookItem.objects.all()
    filter_class = HandbookItemFilter

    def get_serializer_class(self):
        action_map = {
            'retrieve': BaseHandbookItemSerializer,
            'list': BaseHandbookItemSerializer,
            'validate_by_title': ValidateHandbookItemByTitleSerializer,
            'validate_by_version': ValidateHandbookItemByVersionSerializer,
        }
        return action_map.get(self.action, BaseHandbookItemSerializer)

    def get_queryset(self):
        handbook_title = self.request.query_params.get('handbook__title')
        if handbook_title is not None:
            # получение элементов заданного справочника текущей версии
            handbooks = Handbook.objects.filter(title=handbook_title)
            return HandbookItem.objects.filter(
                handbook=handbooks.latest('start_date')) if handbooks.count() != 0 else HandbookItem.objects.none()
        else:
            return super(HandbookItemViewSet, self).get_queryset()

    item_response = openapi.Response('Response description', BaseHandbookItemSerializer)

    @swagger_auto_schema(method='post', responses={200: item_response})
    @action(methods=('POST',), detail=False, serializer_class=ValidateHandbookItemByTitleSerializer)
    def validate_by_title(self, request):
        title = request.data.get('handbook_title')
        value = request.data.get('value')
        contains = request.data.get('contains')

        handbooks = Handbook.objects.filter(title=title)

        if handbooks.count() == 0:
            return Response(data=f'Handbook with title "{title}" not found.', status=status.HTTP_404_NOT_FOUND)

        if contains:
            items = HandbookItem.objects.filter(Q(handbook=handbooks.latest('start_date')) & Q(value__icontains=value))
        else:
            items = HandbookItem.objects.filter(Q(handbook=handbooks.latest('start_date')) & Q(value__iexact=value))

        if items:
            serializer = BaseHandbookItemSerializer(items, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(method='post', responses={200: item_response})
    @action(methods=('POST',), detail=False, serializer_class=ValidateHandbookItemByVersionSerializer)
    def validate_by_version(self, request):
        version = request.data.get('handbook_version')
        contains = request.data.get('contains')
        value = request.data.get('value')

        try:
            handbook = Handbook.objects.get(version=version)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if handbook.count() == 0:
            return Response(data=f'Handbook with version "{version}" not found.', status=status.HTTP_404_NOT_FOUND)

        if contains:
            items = HandbookItem.objects.filter(Q(handbook=handbook) & Q(value__icontains=value))
        else:
            items = HandbookItem.objects.filter(Q(handbook=handbook) & Q(value__iexact=value))

        if items:
            serializer = BaseHandbookItemSerializer(items, many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_404_NOT_FOUND)
