from django.contrib import admin

from handbook.models import HandbookItem


@admin.register(HandbookItem)
class HandbookItemAdmin(admin.ModelAdmin):
    fields = ('handbook',
              'code',
              'value',)
    search_fields = ('value',
                     'handbook__title',
                     'handbook__short_title',
                     'code',)
