from django.contrib import admin

from handbook.models import Handbook, HandbookItem


class HandbookItemInline(admin.TabularInline):
    model = HandbookItem
    fields = ('code',
              'value',)
    extra = 0


@admin.register(Handbook)
class HandbookAdmin(admin.ModelAdmin):
    inlines = (HandbookItemInline,)
    list_display = ('title',
                    'short_title',
                    'version',
                    'start_date',
                    'description',)
    search_fields = ('title',
                     'short_title',
                     'version',
                     'description',)
