from rest_framework import serializers

from handbook.models import Handbook


class BaseHandbookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Handbook
        fields = ('id',
                  'title',
                  'short_title',
                  'description',
                  'version',
                  'start_date',)
