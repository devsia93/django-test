from rest_framework import serializers

from handbook.models import HandbookItem


class BaseHandbookItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = HandbookItem
        fields = ('id',
                  'handbook',
                  'code',
                  'value',)


class ValidateHandbookItemByVersionSerializer(serializers.ModelSerializer):
    handbook_version = serializers.CharField(write_only=True)
    contains = serializers.BooleanField(write_only=True)

    class Meta:
        model = HandbookItem
        fields = ('handbook_version',
                  'value',
                  'contains',)


class ValidateHandbookItemByTitleSerializer(serializers.ModelSerializer):
    handbook_title = serializers.CharField(write_only=True)
    contains = serializers.BooleanField(write_only=True)

    class Meta:
        model = HandbookItem
        fields = ('handbook_title',
                  'value',
                  'contains',)
