from django.db import models

from handbook.models import Handbook


class HandbookItem(models.Model):
    """
    Элемент справочника.
    """
    handbook = models.ForeignKey(Handbook, on_delete=models.CASCADE, related_name='handbook_items',
                                 verbose_name='Справочник')
    code = models.CharField(max_length=16, verbose_name='Код')
    value = models.CharField(max_length=64, db_index=True, verbose_name='Значение')

    class Meta:
        verbose_name = 'Элемент справочника'
        verbose_name_plural = 'Элементы справочника'

    def __str__(self):
        return self.value
