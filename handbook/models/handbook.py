from django.db import models


class Handbook(models.Model):
    """
    Справочник.
    """
    title = models.CharField(max_length=32, verbose_name='Наименование')
    short_title = models.CharField(max_length=16, blank=True, default='', verbose_name='Короткое наименование')
    description = models.TextField(blank=True, default='', verbose_name='Описание')
    version = models.CharField(max_length=16, unique=True, verbose_name='Версия')
    start_date = models.DateField(blank=True, null=True, db_index=True,
                                  verbose_name='Дата начала действия справочника этой версии')

    class Meta:
        verbose_name = 'Справочник'
        verbose_name_plural = 'Справочники'

    def __str__(self):
        return self.short_title if self.short_title else self.title
